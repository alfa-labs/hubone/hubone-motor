﻿using JUST;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Motor.Parser.Models;

namespace Motor.Parser
{
    public class IntegrationDB
    {
        public static string input = "";
        public static string transformer = "";
        //public static string transformedString = "";
        public static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Json\\IntegrationDB\\";
        public static integration_entity objIntegration;


        public static integration_entity ProcessIntegration(string body)
        {
            objIntegration = new integration_entity();
            input = body;// File.ReadAllText(path + "Integration_Input");

            try
            {

                //rules            
                transformer = File.ReadAllText(path + "rules_Transform");
                JsonTransformer.Transform(transformer, input);

                //verb
                transformer = File.ReadAllText(path + "verb_Transform");
                JsonTransformer.Transform(transformer, input);

                //endpoint
                transformer = File.ReadAllText(path + "endpoint_Transform");
                JsonTransformer.Transform(transformer, input);

                //synchronous
                transformer = File.ReadAllText(path + "synchronous_Transform");
                JsonTransformer.Transform(transformer, input);

                Console.WriteLine("Retornando objeto--> " + objIntegration.ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objIntegration;
        }

        public static void processRules(string json)
        {
            string jsonProcess = json;
            Console.WriteLine("Integration : rules--> " + jsonProcess);

            objIntegration.rules = jsonProcess;
        }

        public static void processVerb(string json)
        {
            string jsonProcess = json;
            Console.WriteLine("Integration : verb--> " + jsonProcess);

            objIntegration.verb = jsonProcess;
        }

        public static void processEndPoint(string json)
        {
            string jsonProcess = json;
            Console.WriteLine("Integration : endpoint--> " + jsonProcess);

            objIntegration.endpoint = jsonProcess;
        }

        public static void processSynchronous(bool json)
        {
            bool jsonProcess = json;
            Console.WriteLine("Integration : synchronous--> " + jsonProcess);

            objIntegration.synchronous = jsonProcess;
        }
    }


}
