﻿using Sap.Data.Hana;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor.Parser.Util
{
    public class Hana
    {
        private static HanaConnection Connection = new HanaConnection();
        private readonly HanaDataAdapter DataAdapter = new HanaDataAdapter();
        private HanaDataReader DataReader;
        private HanaCommand Command;
        private readonly HanaTransaction Transaction;
        private readonly string ConnectionString;

        public Hana()
        {
            if (String.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = $"Server=hanab1:30015;UserID=SYSTEM;Password=!Acesso@2019";
            }
        }

        public string GetConnectedServer()
        {
            return Connection.DataSource;
        }

        public void Connect()
        {
            //string conexao = $"Server=bresdvpsap-01:30015;UserID=SYSTEM;Password=Prop@v1409";
            //HanaConnection _conn = new HanaConnection(conexao);
            if (Connection == null || !ConnectionString.StartsWith(ConnectionString))
            {
                Connection = new HanaConnection();
            }

            if (Connection.State == ConnectionState.Broken || Connection.State == ConnectionState.Closed)
            {
                try
                {
                    Connection.ConnectionString = ConnectionString;
                    Connection.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao conectar Hana: " + ex.Message);
                }
            }
        }

        public void Disconnect()//HanaConnection conn
        {
            //string conexao = $"Server=bresdvpsap-01:30015;UserID=SYSTEM;Password=Prop@v1409";
            //HanaConnection _conn = new HanaConnection(conexao);
            if (Connection != null && Connection.State == ConnectionState.Open)
            {
                try
                {
                    Connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Erro ao conectar Hana: " + ex.Message);
                }
            }

            //if (conn.State == ConnectionState.Open)
            //{
            //    try
            //    {
            //        conn.Close();
            //    }
            //    catch (Exception ex)
            //    {
            //        throw new Exception("Erro ao conectar Hana: " + ex.Message);
            //    }
            //}
        }

        public void Close()
        {
            if (Connection.State == ConnectionState.Open || Connection.State == ConnectionState.Executing || Connection.State == ConnectionState.Fetching)
            {
                Connection.Close();
                Connection.Dispose();
                Connection = null;
            }
        }

        public async Task<object> ExecuteScalarAsync(string query)
        {
            try
            {
                Connect();

                Command = new HanaCommand(query, Connection)
                {
                    CommandTimeout = 120,
                    CommandType = CommandType.Text,
                    Transaction = Transaction
                };

                return await Command.ExecuteScalarAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Erro ao executar HanaDataReaderAsync: " + ex.Message);
            }
        }

        public async Task<HanaDataReader> ExecuteReaderAsync(string query)
        {
            try
            {
                Connect();

                Command = new HanaCommand(query, Connection)
                {
                    CommandTimeout = 120,
                    CommandType = CommandType.Text,
                    Transaction = Transaction
                };
                DataReader = (HanaDataReader)await Command.ExecuteReaderAsync();

                //Disconnect();

                return DataReader;
            }
            catch (Exception ex)
            {
                // Disconnect();
                throw new Exception("Erro ao executar HanaDataReaderAsync: " + ex.Message);
            }

        }

        public HanaDataReader ExecuteReader(string query)
        {
            try
            {
                Connect();

                Command = new HanaCommand(query, Connection)
                {
                    CommandTimeout = 120,
                    CommandType = CommandType.Text,
                    Transaction = Transaction
                };
                DataReader = (HanaDataReader)Command.ExecuteReader();

                //Disconnect();

                return DataReader;
            }
            catch (Exception ex)
            {
                // Disconnect();
                throw new Exception("Erro ao executar HanaDataReaderAsync: " + ex.Message);
            }

        }

        public async Task ExecuteNonQueryAsync(string query)
        {
            try
            {
                Connect();

                Command = new HanaCommand(query, Connection)
                {
                    CommandTimeout = 120,
                    CommandType = CommandType.Text,
                    Transaction = Transaction
                };

                await Command.ExecuteNonQueryAsync();

                // Disconnect();
            }
            catch (Exception ex)
            {
                // Disconnect();
                throw new Exception("Erro ao executar HanaDataReaderAsync: " + ex.ToString());
            }
        }
    }
}
