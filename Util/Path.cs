﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Motor.Parser.Util
{
    public class Path
    {
        public static string getRootPath(string rootFilename)
        {
            string _root;

            try
            {
                var rootDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase);
                Regex matchThepath = new Regex(@"(?<!fil)[A-Za-z]:\\+[\S\s]*?(?=\\+bin)");
                var appRoot = matchThepath.Match(rootDir).Value;
                _root = appRoot + "\\" + rootFilename;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _root;
        }
    }
}
