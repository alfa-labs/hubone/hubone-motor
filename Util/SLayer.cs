﻿using B1SLayer;
using Microsoft.Extensions.Configuration;
using Motor.Parser.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sap.Data.Hana;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Text;

namespace Motor.Parser.Util
{
    public class SLayer
    {

        public static IConfiguration _configuration;

        public static SLConnection LoginSL()
        {
            var serviceLayer = new SLConnection("https://hanab1:50000/b1s/v1", "SBO_MATRIX_PRD", "manager", "B1admin@");

            return serviceLayer;
        }

        public static DataTable ExecuteQuerySLDataTable(string query)
        {
            DataTable dt = new DataTable();
            _configuration = new ConfigurationBuilder().AddJsonFile(Util.Path.getRootPath("appsettings.json")).Build();
            string connStringDBHana = _configuration.GetSection("DbConnectionConfig")["ConnectionStringDBHANA"];
            string ret = "";

            try
            {

                var hana = new Hana();


                using (var dr = hana.ExecuteReader(query))
                {
                    if (dr.HasRows)
                    {
                        dt = new DataTable();
                        dt.Load(dr);

                        foreach (DataRow row in dt.Rows)
                        {
                            ret = row["CardCode"].ToString();
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;

        }

        public static dynamic Login(string SL, string User, string Pass, string DataBase)
        {

            var httpWebRequest =
                (HttpWebRequest)WebRequest.Create(SL + "/Login");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.Timeout = 300 * 10000;
            httpWebRequest.ServicePoint.Expect100Continue = false;
            httpWebRequest.CookieContainer = new CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += BypassSslCallback; //BypassSslCallback;

            var dados = JsonConvert.SerializeObject(new
            {
                CompanyDB = DataBase,
                Password = Pass,
                UserName = User
            });
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(dados);
                streamWriter.Flush();
                streamWriter.Close();
            }

            HttpWebResponse response1 = null;

            try
            {
                response1 = httpWebRequest.GetResponse() as HttpWebResponse;
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    var httpResponse = (HttpWebResponse)response;

                    using (Stream data = response.GetResponseStream())
                    {
                        StreamReader sr = new StreamReader(data);

                        dynamic Dadoserror = Newtonsoft.Json.JsonConvert.DeserializeObject(sr.ReadToEnd());
                        var messageErro = Dadoserror.error.message;
                        return Convert.ToString(messageErro["value"]);
                    }
                }

            }

            string responseContent = null;
            dynamic retorno = "";
            using (var reader = new StreamReader(response1.GetResponseStream(), Encoding.UTF8))
            {
                responseContent = reader.ReadToEnd();
                retorno = Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent);


            }

            return retorno;
        }

        public static dynamic Logout(string serviceLayerAddress, string session)
        {
            var httpWebRequest =
                (HttpWebRequest)WebRequest.Create(serviceLayerAddress +
                                                   string.Format(@"{0}", "/Logout"));//string logout, tira o cookie
                                                                                     //httpWebRequest.Headers.Add("B1S-ReplaceCollectionsOnPatch","true");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.Timeout = 30 * 20000;
            httpWebRequest.ServicePoint.Expect100Continue = false;
            httpWebRequest.CookieContainer = new CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += BypassSslCallback;
            httpWebRequest.CookieContainer.Add(httpWebRequest.RequestUri,
                        new Cookie("B1SESSION", session));
            httpWebRequest.CookieContainer.Add(httpWebRequest.RequestUri,
                       new Cookie("ROUTEID", ".node1"));

            HttpWebResponse response1 = null;

            try
            {
                response1 = httpWebRequest.GetResponse() as HttpWebResponse;
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    var httpResponse = (HttpWebResponse)response;

                    using (Stream data = response.GetResponseStream())
                    {
                        StreamReader sr = new StreamReader(data);

                        dynamic Dadoserror = Newtonsoft.Json.JsonConvert.DeserializeObject(sr.ReadToEnd());
                        var messageErro = Dadoserror.error.message;
                        //return Convert.ToString(messageErro["value"]);
                        throw new Exception("erro: " + Convert.ToString(messageErro["value"]));
                    }
                }

            }




            string responseContent = null;
            dynamic json = null;
            using (var reader = new StreamReader(response1.GetResponseStream(), Encoding.UTF8))
            {
                responseContent = reader.ReadToEnd();
                json = Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent);

            }

            return "OK";
        }

        public static dynamic Add(string dados, string entidade, string serviceLayerAddress, string session)
        {
            var httpWebRequest =
                (HttpWebRequest)WebRequest.Create(serviceLayerAddress +
                                                   string.Format(@"/{0}", entidade));//string logout, tira o cookie
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.Timeout = 30 * 20000;
            httpWebRequest.ServicePoint.Expect100Continue = false;
            // httpWebRequest.ServicePoint.MaxIdleTime = 30 * 10000;
            httpWebRequest.CookieContainer = new CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += BypassSslCallback;
            httpWebRequest.CookieContainer.Add(httpWebRequest.RequestUri,
                        new Cookie("B1SESSION", session));



            if (!string.IsNullOrEmpty(dados))
            {

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(dados);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            HttpWebResponse response1 = null;

            try
            {
                response1 = httpWebRequest.GetResponse() as HttpWebResponse;
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    var httpResponse = (HttpWebResponse)response;

                    using (Stream data = response.GetResponseStream())
                    {
                        StreamReader sr = new StreamReader(data);

                        dynamic Dadoserror = Newtonsoft.Json.JsonConvert.DeserializeObject(sr.ReadToEnd());
                        var messageErro = Dadoserror.error.message;
                        return "ERRO: " + Convert.ToString(messageErro["value"]);
                    }
                }

            }

            string responseContent = null;
            dynamic json = null;
            using (var reader = new StreamReader(response1.GetResponseStream(), Encoding.UTF8))
            {
                responseContent = reader.ReadToEnd();
                json = Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent);

            }

            return json;
        }

        public static dynamic UpdateSL(string dados, string entidade, string serviceLayerAddress, string session)
        {
            var httpWebRequest =
                (HttpWebRequest)WebRequest.Create(serviceLayerAddress +
                                                   string.Format(@"{0}", "/" + entidade));//string logout, tira o cookie
                                                                                          //httpWebRequest.Headers.Add("B1S-ReplaceCollectionsOnPatch","true");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "PATCH";
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.Timeout = 30 * 20000;
            httpWebRequest.ServicePoint.Expect100Continue = false;
            httpWebRequest.CookieContainer = new CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += BypassSslCallback;
            httpWebRequest.CookieContainer.Add(httpWebRequest.RequestUri,
                        new Cookie("B1SESSION", session));



            if (!string.IsNullOrEmpty(dados))
            {

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(dados);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            HttpWebResponse response1 = null;

            try
            {
                response1 = httpWebRequest.GetResponse() as HttpWebResponse;
            }
            catch (WebException ex)
            {
                using (WebResponse response = ex.Response)
                {
                    var httpResponse = (HttpWebResponse)response;

                    using (Stream data = response.GetResponseStream())
                    {
                        StreamReader sr = new StreamReader(data);

                        dynamic Dadoserror = Newtonsoft.Json.JsonConvert.DeserializeObject(sr.ReadToEnd());
                        var messageErro = Dadoserror.error.message;
                        //return Convert.ToString(messageErro["value"]);
                        throw new Exception("erro: " + Convert.ToString(messageErro["value"]));
                    }
                }

            }




            string responseContent = null;
            dynamic json = null;
            using (var reader = new StreamReader(response1.GetResponseStream(), Encoding.UTF8))
            {
                responseContent = reader.ReadToEnd();
                json = Newtonsoft.Json.JsonConvert.DeserializeObject(responseContent);

            }

            return "OK";
        }
       

        private static bool BypassSslCallback(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
            // throw new NotImplementedException();
        }

       
    }
}
