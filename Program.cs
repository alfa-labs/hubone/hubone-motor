﻿using JUST;
using Motor.Parser.Models;
using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

namespace Motor.Parser
{
    public class Program
    {
        public static integration_entity objIntegration;
        public static string body = "";
        public static string pathIntegration = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Json\\IntegrationDB\\";
        public static string pathProcessRules = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Json\\ProcessRules\\";
        public static void Main(string[] args)
        {
            //Orders - Type 2
            string jsonStringOrders = @"{

                                    ""rules"" : [
                                        {
                                    
                                            ""type"": ""2"",
                                            ""sequence"": 2,
                                            ""verb"":""POST"",
                                            ""endpoint"":""Orders"",

                                            ""origin"" : [
                                                {
                                            ""cod_cliente"": 1,                                                                
                                            ""endereco"": 0,
                                            ""filial"": -1,
                                            ""data_vencimento"": -1,                                    
                                            ""cod_produto"": 0,
                                            ""quantidade"": 0,
                                            ""valor_unitario"": 0  
                                                }
                                            ],

                                            ""field"" : [
                                                {
                                            ""cod_cliente"": ""CardCode"",                                                             
                                            ""endereco"": ""Address"",
                                            ""filial"": ""BPL_IDAssignedToInvoice"",
                                            ""data_vencimento"": ""DocDueDate"",
                                            ""produtos[]"": ""DocumentLines[]"",
                                            ""cod_produto"": ""DocumentLines.ItemCode"",
                                            ""quantidade"": ""DocumentLines.Quantity"",
                                            ""valor_unitario"": ""DocumentLines.UnitPrice""  
                                                }
                                            ],                                           

                                            ""value"" : [
                                                {
                                            ""cod_cliente"": ""@CardCode@"",                                                                
                                            ""endereco"": ""endereco"",
                                            ""filial"": 1,
                                            ""data_vencimento"": ""2022-12-31"",                                    
                                            ""cod_produto"": ""produtos.cod_produto"",
                                            ""quantidade"": ""produtos.quantidade"",
                                            ""valor_unitario"": ""produtos.valor_unitario""
                                                }
                                            ],

                                            ""nextSequence"": [
                                                {
                                            ""sequenceIfOk"": 2,
                                            ""sequenceIfNotOk"": 3
                                                }
                                            ],
                                        },
                                        {                                    
                                            ""type"": ""1"",
                                            ""sequence"": 1,
                                            ""sql"": ""SELECT * FROM SBO_MATRIX_PRD.""OCRD"" INNER JOIN SBO_MATRIX_PRD.""CRD7"" ON ""OCRD"".""CardCode"" = ""CRD7"".""CardCode"" WHERE ""CRD7"".""TaxId0"" = '?' AND ""OCRD"".""CardType"" = '?' AND ""OCRD"".""CardName"" LIKE '%?%'"",
                                            ""onlyOne"":true,
                                            ""origin"" : [
                                                {
                                            ""cnpj_cliente"": 0,                                                                
                                            ""tipoCli"": 0,
                                            ""nome_cliente"": 0,
                                                }
                                            ],
                                            ""order"" : [
                                                {
                                            ""cnpj_cliente"": 1,                                                                
                                            ""tipoCli"": 2,
                                            ""nome_cliente"": 3,
                                                }
                                            ],
                                            ""value"" : [
                                                {
                                            ""cnpj_cliente"": ""@cnpj_cliente@"",                                                                
                                            ""tipoCli"": ""@tipoCli@"",
                                            ""nome_cliente"": ""@nome_cliente@"",
                                                }
                                            ],
                                        },
                                    ],

                                    }";

            List<rules> listRules = new List<rules>();
            List<BodyParameter> listBodyParameters = new List<BodyParameter>();
            BodyParameter bodyParameter = null;
            rules regra = null;


            dynamic dynamicJson = JsonConvert.DeserializeObject<ExpandoObject>(jsonStringOrders, new ExpandoObjectConverter());

            foreach (KeyValuePair<string, object> kvp in dynamicJson)
            {
                if ((kvp.Key.ToString().Equals("rules")))
                    regra = new rules(); 
                if ((kvp.Key.ToString().Equals("origin")))
                    bodyParameter = new BodyParameter();

                if (kvp.Value.GetType().ToString().Contains("List"))
                {
                    dynamic d2 = kvp.Value;
                    foreach (ExpandoObject d in d2)
                    {
                        foreach (KeyValuePair<string, object> kvp2 in d)
                        {

                            
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("rules|type"))
                            {
                                regra.type = kvp2.Value.ToString();
                            }
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("rules|sequence"))
                            {
                                regra.sequence = Convert.ToInt32(kvp2.Value.ToString());
                            }
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("rules|verb"))
                            {
                                regra.serviceLayer.verb = kvp2.Value.ToString();
                            }
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("rules|endpoint"))
                            {
                                regra.serviceLayer.endpoint  = kvp2.Value.ToString();
                            }

                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("origin"))//|cod_cliente"))//nao pode ter nada fixo no codigo (ex: cod_cliente")
                            {  
                                //loop origin
                                bodyParameter.origin = Convert.ToInt32(kvp2.Value.ToString());
                                regra.serviceLayer.bodyParameters.Add(bodyParameter);
                            }
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("field"))
                            {
                                //loop field
                                bodyParameter.field = kvp2.Value.ToString();
                                regra.serviceLayer.bodyParameters.Add(bodyParameter);
                            }
                            if ((kvp.Key.ToString() + "|" + kvp2.Key.ToString()).Equals("value"))
                            {
                                //loop value
                                bodyParameter.value = kvp2.Value.ToString();
                                regra.serviceLayer.bodyParameters.Add(bodyParameter);
                            }

                            Console.WriteLine(kvp2.Key + ": " + kvp2.Value);
                        }
                    }
                    listRules.Add(regra);
                }
                else
                {                    
                    Console.WriteLine(kvp.Key + ": " + kvp.Value);
                }
            }


            


            Console.WriteLine("INICIO INTEGRATIONDB : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff tt"));
            #region Integration - Processa JSON e retorna objeto para salvar na base de dados 
            body = File.ReadAllText(pathIntegration + "Integration_Input");
            objIntegration = IntegrationDB.ProcessIntegration(body);
            #endregion

            Console.WriteLine("INICIO PROCESSORDERRULES : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff tt"));
            #region ProcessRulesOrders - Processa Regras contidas no JSON
            body = File.ReadAllText(pathProcessRules + "payLoadOrders2");//payLoadOrders2
            ProcessRulesOrders.ProcessRules(objIntegration, body);
            #endregion           

        }

        public static IEnumerable<KeyValuePair<string, string>> StringProperties(object obj)
        {
            return from p in obj.GetType().GetProperties()
                   where p.PropertyType == typeof(string)
                   select new KeyValuePair<string, string>(p.Name, (string)p.GetValue(obj));
        }

    }
}
