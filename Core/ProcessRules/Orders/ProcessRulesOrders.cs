﻿using JUST;
using Microsoft.Extensions.Configuration;
using Motor.Parser.Models;
using Motor.Parser.Models.SAP;
using Motor.Parser.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;


namespace Motor.Parser
{
    public class ProcessRulesOrders
    {
        public static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\Json\\ProcessRules\\";
        public static List<rules> listRules = new List<rules>();
        public static rules rules = new rules();
        public static string jsonRules = "";
        public static string jsonOrders = "";
        public static string paramValue = "";
        public static DataTable dt = new DataTable();
        public static IConfiguration _configuration;

        public static void ProcessRules(integration_entity payLoadRules, string payLoadOrders)
        {

            try
            {
                jsonOrders = payLoadOrders;
                jsonRules = payLoadRules.rules;
                ArrayRegras(jsonRules);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static DataTable processType1(rules regras)//query
        {

            string sQuery = "";

            try
            {
                sQuery = trataParametrosQuery(regras);
                dt = SLayer.ExecuteQuerySLDataTable(sQuery);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        public static string processType2(rules regras)
        {
            string retOKOuErro = "";
            Orders orders = new Orders();
            List<DocumentLines> listaLinhas = new List<DocumentLines>();
            DocumentLines ordersLines = null;
            BusinessPartners bp = new BusinessPartners();
            List<BPFiscalTaxIDCollection> listaLinhasBP = new List<BPFiscalTaxIDCollection>();
            BPFiscalTaxIDCollection bpLines = null;
            List<BodyParameter> bodyParameters = regras.serviceLayer.bodyParameters;
            List<dynamic> listValorString = new List<dynamic>();
            dynamic retornoSL = null;
            string endpoint = "";
            string verb = "";

            try
            {

                endpoint = regras.serviceLayer.endpoint;
                verb = regras.serviceLayer.verb;

                //loop na lista acima que vai comparar o campo "origin" pra ver 0:pega payLoad enviado - 1: pega da query  -1:chumbado (pega valor da tag "value")
                if (endpoint.Equals("Orders"))
                {
                    foreach (var item in bodyParameters)
                    {
                        switch (item.origin)
                        {
                            case 0://primeiro item no Array DocumentLines de bodyParameters
                                if (item.field.Contains("ItemCode"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                    {
                                        ordersLines = new DocumentLines();
                                        ordersLines.ItemCode = strVlr;
                                        listaLinhas.Add(ordersLines);
                                    }

                                }
                                if (item.field.Contains("Quantity"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhas)
                                        {
                                            if (linha.Quantity == 0)
                                            {
                                                listaLinhas[cont].Quantity = Convert.ToInt32(strVlr);
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("UnitPrice"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhas)
                                        {
                                            if (linha.UnitPrice == 0)
                                            {
                                                listaLinhas[cont].UnitPrice = Convert.ToDecimal(strVlr);
                                                cont++;
                                            }
                                        }
                                    }
                                }

                                if (item.field.Contains("DocDueDate"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                        orders.DocDueDate = strVlr;
                                }


                                if (item.field.Contains("BPL_IDAssignedToInvoice"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                        orders.BPL_IDAssignedToInvoice = strVlr;
                                }

                                if (item.field.Contains("CardCode"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "Orders");
                                    foreach (var strVlr in listValorString)
                                        orders.CardCode = strVlr;
                                }


                                break;
                            case 1:
                                if (item.field.Contains("ItemCode"))
                                    ordersLines.ItemCode = dt.Rows[0]["ItemCode"].ToString();
                                if (item.field.Contains("Quantity"))
                                    ordersLines.Quantity = Convert.ToInt32(dt.Rows[0]["Quantity"].ToString());
                                if (item.field.Contains("UnitPrice"))
                                    ordersLines.UnitPrice = Convert.ToDecimal(dt.Rows[0]["UnitPrice"].ToString());
                                if (item.field.Contains("CardCode"))
                                    orders.CardCode = dt.Rows[0]["CardCode"].ToString();
                                if (item.field.Contains("DocDueDate"))
                                    orders.DocDueDate = dt.Rows[0]["DocDueDate"].ToString();
                                if (item.field.Contains("BPL_IDAssignedToInvoice"))
                                    orders.BPL_IDAssignedToInvoice = Convert.ToInt32(dt.Rows[0]["BPL_IDAssignedToInvoice"].ToString());

                                break;
                            case -1:
                                if (item.field.Contains("ItemCode"))
                                    ordersLines.ItemCode = item.value.ToString();
                                if (item.field.Contains("Quantity"))
                                    ordersLines.Quantity = Convert.ToInt32(item.value.ToString());
                                if (item.field.Contains("UnitPrice"))
                                    ordersLines.UnitPrice = Convert.ToDecimal(item.value.ToString());
                                if (item.field.Contains("BPL_IDAssignedToInvoice"))
                                    orders.BPL_IDAssignedToInvoice = Convert.ToInt32(item.value.ToString());
                                if (item.field.Contains("DocDueDate"))
                                    orders.DocDueDate = item.value.ToString();
                                if (item.field.Contains("CardCode"))
                                    orders.CardCode = item.value.ToString();
                                break;
                        }
                    }

                    orders.DocumentLines = listaLinhas;

                    //pega JSON obtido nas operações acima,converte pro formato POST/PATCH/PUT(ja feito outro proj)(ver delete tb) e seta variavel declarada no escopo la em cima
                    var jsonSL = JsonConvert.SerializeObject(orders, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        Formatting = Formatting.Indented
                    });

                    Console.WriteLine("Orders objeto--> SL " + jsonSL);
                    retornoSL = ExecutaRequisicaoFinalSL(jsonSL, verb, endpoint);

                }

                if (endpoint.Equals("BusinessPartners"))
                {
                    foreach (var item in bodyParameters)
                    {
                        switch (item.origin)
                        {
                            case 0://primeiro item no Array BPFiscalTaxIDCollection de bodyParameters
                                if (item.field.Contains("TaxId0"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        bpLines = new BPFiscalTaxIDCollection();
                                        bpLines.TaxId0 = strVlr;
                                        listaLinhasBP.Add(bpLines);
                                    }

                                }
                                if (item.field.Contains("TaxId4"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhasBP)
                                        {
                                            if (String.IsNullOrEmpty(linha.TaxId4))
                                            {
                                                listaLinhasBP[cont].TaxId4 = strVlr;
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("TaxId5"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhasBP)
                                        {
                                            if (String.IsNullOrEmpty(linha.TaxId5))
                                            {
                                                listaLinhasBP[cont].TaxId5 = strVlr;
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("Address"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhasBP)
                                        {
                                            if (String.IsNullOrEmpty(linha.Address))
                                            {
                                                listaLinhasBP[cont].Address = strVlr;
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("BPCode"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhasBP)
                                        {
                                            if (String.IsNullOrEmpty(linha.BPCode))
                                            {
                                                listaLinhasBP[cont].BPCode = strVlr;
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("AddrType"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                    {
                                        int cont = 0;
                                        foreach (var linha in listaLinhasBP)
                                        {
                                            if (String.IsNullOrEmpty(linha.AddrType))
                                            {
                                                listaLinhasBP[cont].AddrType = strVlr;
                                                cont++;
                                            }
                                        }
                                    }
                                }
                                if (item.field.Contains("CardType"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                        bp.CardType = strVlr;
                                }
                                if (item.field.Contains("CardName"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                        bp.CardName = strVlr;
                                }

                                if (item.field.Contains("CardCode"))
                                {
                                    listValorString = BuscaDadosPayLoad(item.value, "BusinessPartners");
                                    foreach (var strVlr in listValorString)
                                        bp.CardCode = strVlr;
                                }


                                break;
                            case 1:
                                if (item.field.Contains("CardCode"))
                                    bp.CardCode = dt.Rows[0]["CardCode"].ToString();
                                if (item.field.Contains("CardName"))
                                    bp.CardName = dt.Rows[0]["CardName"].ToString();
                                if (item.field.Contains("TaxId0"))
                                    bpLines.TaxId0 = dt.Rows[0]["TaxId0"].ToString();
                                if (item.field.Contains("Address"))
                                    bpLines.Address = dt.Rows[0]["Address"].ToString();
                                if (item.field.Contains("BPCode"))
                                    bpLines.BPCode = dt.Rows[0]["BPCode"].ToString();
                                if (item.field.Contains("CardType"))
                                    bp.CardType = dt.Rows[0]["CardType"].ToString();
                                if (item.field.Contains("TaxId4"))
                                    bpLines.TaxId4 = dt.Rows[0]["TaxId4"].ToString();
                                if (item.field.Contains("TaxId5"))
                                    bpLines.TaxId5 = dt.Rows[0]["TaxId5"].ToString();
                                if (item.field.Contains("AddrType"))
                                    bpLines.AddrType = dt.Rows[0]["AddrType"].ToString();
                                break;
                            case -1:
                                if (item.field.Contains("TaxId0"))
                                    bpLines.TaxId0 = item.value.ToString();
                                if (item.field.Contains("TaxId4"))
                                    bpLines.TaxId4 = item.value.ToString();
                                if (item.field.Contains("TaxId5"))
                                    bpLines.TaxId5 = item.value.ToString();
                                if (item.field.Contains("Address"))
                                    bpLines.Address = item.value.ToString();
                                if (item.field.Contains("AddrType"))
                                    bpLines.AddrType = item.value.ToString();
                                if (item.field.Contains("BPCode"))
                                    bpLines.BPCode = item.value.ToString();
                                if (item.field.Contains("CardType"))
                                    bp.CardType = item.value.ToString();
                                if (item.field.Contains("CardName"))
                                    bp.CardName = item.value.ToString();
                                if (item.field.Contains("CardCode"))
                                    bp.CardCode = item.value.ToString();
                                break;
                        }
                    }

                    bp.BPFiscalTaxIDCollection = listaLinhasBP;
                    //pega JSON obtido nas operações acima,converte pro formato POST/PATCH/PUT(ja feito outro proj)(ver delete tb) e seta variavel declarada no escopo la em cima
                    var jsonSL = JsonConvert.SerializeObject(bp, new JsonSerializerSettings()
                    {
                        NullValueHandling = NullValueHandling.Ignore,
                        Formatting = Formatting.Indented
                    });

                    Console.WriteLine("BusinessPartners objeto--> SL " + jsonSL);
                    retornoSL = ExecutaRequisicaoFinalSL(jsonSL, verb, endpoint);

                }


            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (retornoSL.ToString().Contains("ERRO"))
                retOKOuErro = "ERRO";
            else
                retOKOuErro = "OK";

            return retOKOuErro;
        }

        public static void ArrayRegras(string json)
        {
            string retOKOuErro = "";

            try
            {

                listRules = JsonUtil.ConverteJSonParaObject<List<rules>>(json);


                foreach (rules regra in listRules)
                {
                    switch (regra.type)
                    {
                        case "1":
                            dt.Clear();
                            dt = processType1(regra);
                            if (dt.Rows.Count > 0)
                                retOKOuErro = "OK";
                            break;
                        case "2":
                            retOKOuErro = processType2(regra);
                            break;
                    }
                    //le a sequencia do JSON pra seguir o fluxo chama metodo ProcessaSequencia(regras.sequence.ToString()) (decide pra onde vai o processo - chama processtype 1 ou 2 çor enquanto)
                    ProcessaSequencia(regra, retOKOuErro, listRules);

                    break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static void ProcessaSequencia(rules regra, string status, List<rules> listRegras)
        {
            string retOKOuErro = "";
            string sequencia = "";
            string jsonSL = "";

            try
            {

                //verifica sequencia se OK ou Nao e chama metodos Processtype 1 ou 2 por enquanto (depois ve o 3 API)
                if (status.Equals("OK"))
                    sequencia = regra.nextSequence.sequenceIfOk.ToString();
                else
                    sequencia = regra.nextSequence.sequenceIfNotOk.ToString();

                //se nao tiver mais sequencia p seguir pega JSON convertido (variavel setada em processType2) executa metodo. ExecutaRequisicaoSL
                if (String.IsNullOrEmpty(sequencia) || sequencia.Equals("0"))
                {
                    Console.WriteLine("FIM : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff tt"));
                    Environment.Exit(1);
                }

                // usa LINQ pra pegar regra relativa a "sequence" = variavel sequencia (acima), colocar num outro list que sera chamado abaixo no switch
                List<rules> nextRule = listRegras.Where(m => m.sequence.ToString() == sequencia)
                                                .Select(m => new rules
                                                {
                                                    type = m.type,
                                                    query = m.query,
                                                    sequence = m.sequence,
                                                    nextSequence = m.nextSequence,
                                                    serviceLayer = m.serviceLayer,
                                                    conditionToSuccess = m.conditionToSuccess
                                                }).ToList<rules>();


                switch (nextRule[0].type)//sequencia
                {

                    case "1":
                        dt.Clear();
                        dt = processType1(nextRule[0]);
                        if (dt.Rows.Count > 0)
                            retOKOuErro = "OK";
                        break;
                    case "2":
                        retOKOuErro = processType2(nextRule[0]);
                        break;
                }



                ProcessaSequencia(nextRule[0], retOKOuErro, listRegras);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public static string trataParametrosQuery(rules regra)
        {

            string transformer = "";
            string strFormatada = "";
            string parametro = "";
            string sQuery = "";

            try
            {

                List<Parameter> listParameters = new List<Parameter>();
                listParameters = regra.query.parameters;

                int cont = 0;
                foreach (Parameter param in listParameters)
                {
                    transformer = File.ReadAllText(path + "transformParam");
                    parametro = param.value.Replace("@", "");
                    strFormatada = transformer.Replace("{0}", parametro);
                    JsonTransformer.Transform(strFormatada, jsonOrders);

                    if (cont == 0)
                        sQuery = regra.query.sql.ToString().ReplaceFirst("?", paramValue);
                    else
                        sQuery = sQuery.ReplaceFirst("?", paramValue);

                    cont++;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return sQuery;
        }

        public static void transformParamQuery(string json)
        {
            paramValue = json;

        }


        public static List<dynamic> BuscaDadosPayLoad(object valor, string tipoObj)
        {

            List<dynamic> ret = new List<dynamic>();
            var json = jsonOrders;

            try
            {

                ret = transformBodyParameters(valor, tipoObj);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }

        public static List<dynamic> transformBodyParameters(object valor, string tipoObj)
        {
            string transformer = "";
            List<dynamic> ret = new List<dynamic>();
            string output = "";
            List<ParentProps> props = null;
            ParentProps parentProps = new ParentProps();


            try
            {
                switch (tipoObj)
                {

                    case "Orders":
                        if (valor.ToString().Contains("cod_produto"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_Produtos");
                        if (valor.ToString().Contains("quantidade"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_Qtd");
                        if (valor.ToString().Contains("valor_unitario"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_VlrUnit");
                        if (valor.ToString().Contains("data_vencimento"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_DataVenc");
                        //if (valor.ToString().Contains("filial"))
                        //    transformer = File.ReadAllText(path + "Transformer_Orders_SAP_Filial");

                        output = JsonTransformer.Transform(transformer, jsonOrders);
                        output = output.Substring(10, output.Length - 11);
                        var objs = JsonConvert.DeserializeObject<List<ParentProps>>(output);
                        props = JsonConvert.DeserializeObject<List<ParentProps>>(output);

                        foreach (ParentProps item in props)
                            ret.Add(item.Property);

                        break;

                    case "BusinessPartners":
                        if (valor.ToString().Contains("cod_cliente"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_CodCliente");
                        if (valor.ToString().Contains("nome_cliente"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_NomeCliente");
                        if (valor.ToString().Contains("cnpj_cliente"))
                            transformer = File.ReadAllText(path + "Transformer_Orders_SAP_CNPJ");

                        output = JsonTransformer.Transform(transformer, jsonOrders);
                        output = output.Substring(10, output.Length - 11);

                        parentProps = JsonConvert.DeserializeObject<ParentProps>(output);

                        ret.Add(parentProps.Property);

                        break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ret;
        }

        public static dynamic ExecutaRequisicaoFinalSL(string json, string tipoRequisicao, string entidade)
        {
            dynamic dadosRequisicao = null;
            _configuration = new ConfigurationBuilder().AddJsonFile(Util.Path.getRootPath("appsettings.json")).Build();
            string ServiceLayerURL = _configuration.GetSection("DbConnectionConfig")["ServiceLayerURL"];
            string B1User = _configuration.GetSection("DbConnectionConfig")["B1User"];
            string B1Password = _configuration.GetSection("DbConnectionConfig")["B1Password"];
            string Database = _configuration.GetSection("DbConnectionConfig")["Database"];

            try
            {
                dynamic retornoLogin = SLayer.Login(ServiceLayerURL, B1User, B1Password, Database);
                string sessionId = retornoLogin.SessionId;

                switch (tipoRequisicao)
                {

                    case "POST":
                        dadosRequisicao = SLayer.Add(json, entidade, ServiceLayerURL, sessionId);
                        break;
                    case "PATCH":
                        dadosRequisicao = SLayer.UpdateSL(json, entidade, ServiceLayerURL, sessionId);
                        break;
                }



                Console.WriteLine("Retorno SL " + "-" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff tt") + dadosRequisicao.ToString());

                dynamic retornoLogout = SLayer.Logout(ServiceLayerURL, sessionId);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dadosRequisicao;

        }

        public static string ConverteJSON(string jsonEnviado)
        {
            string jsonRequisicaoSL = "";

            return jsonRequisicaoSL;
        }


    }


}
