﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models
{
    public class Field
    {
        //public Field(string name, Type type)
        //{
        //    this.FieldName = name;
        //    this.FieldType = type;
        //}

        //public string FieldName;

        //public Type FieldType;

        private string _fieldName;
        private Type _fieldType;

        public string FieldName { get => _fieldName; set => _fieldName = value; }
        public Type FieldType { get => _fieldType; set => _fieldType = value; }
    }
}
