﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models
{
    public class entrypoint_entity
    {
        private string _payload;
        private string _queryParam;
        private string _pathParam;
        private string _status;
        private int _integrationId;

        public string payload { get => _payload; set => _payload = value; }
        public string queryParam { get => _queryParam; set => _queryParam = value; }
        public string pathParam { get => _pathParam; set => _pathParam = value; }
        public string status { get => _status; set => _status = value; }
        public int integrationId { get => _integrationId; set => _integrationId = value; }
    }
}
