﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models.SAP
{
    public class BusinessPartners
    {
        private string cardCode;
        private string cardName;
        private string cardType;
        public string CardCode { get => cardCode; set => cardCode = value; }
        public string CardName { get => cardName; set => cardName = value; }
        public string CardType { get => cardType; set => cardType = value; }
        public List<BPFiscalTaxIDCollection> BPFiscalTaxIDCollection { get => bPFiscalTaxIDCollection; set => bPFiscalTaxIDCollection = value; }

        private List<BPFiscalTaxIDCollection> bPFiscalTaxIDCollection;

    }

    public class BPFiscalTaxIDCollection
    {
        private string taxId0;
        private string taxId4;
        private string taxId5;
        private string address;
        private string addrType;
        private string bPCode;

        public string TaxId0 { get => taxId0; set => taxId0 = value; }
        public string TaxId4 { get => taxId4; set => taxId4 = value; }
        public string TaxId5 { get => taxId5; set => taxId5 = value; }
        public string Address { get => address; set => address = value; }
        public string AddrType { get => addrType; set => addrType = value; }
        public string BPCode { get => bPCode; set => bPCode = value; }
    }
}
