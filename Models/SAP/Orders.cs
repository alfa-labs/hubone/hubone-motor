﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models.SAP
{
    public class Orders
    {
        private string cardCode;
        public string CardCode { get => cardCode; set => cardCode = value; }

        private string docDueDate;

        private int bPL_IDAssignedToInvoice;

        private List<DocumentLines> documentLines;
        public List<DocumentLines> DocumentLines { get => documentLines; set => documentLines = value; }
        public string DocDueDate { get => docDueDate; set => docDueDate = value; }
        public int BPL_IDAssignedToInvoice { get => bPL_IDAssignedToInvoice; set => bPL_IDAssignedToInvoice = value; }
    }

    public class DocumentLines
    {
        private string itemCode;
        private int quantity;
        private decimal unitPrice;

        public string ItemCode { get => itemCode; set => itemCode = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public decimal UnitPrice { get => unitPrice; set => unitPrice = value; }
    }
}
