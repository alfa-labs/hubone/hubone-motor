﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models
{
    public class rulesType2
    {
        private string _serviceLayer;
        private string _verb;
        private string _endpoint;
        private string _sequence;
        private string _nextSequence;
        private string _sequenceIfOK;
        private string _sequenceIfNotOK;

        private List<bodyParameters> _listParameters;

        public string serviceLayer { get => _serviceLayer; set => _serviceLayer = value; }
        public string verb { get => _verb; set => _verb = value; }
        public string endpoint { get => _endpoint; set => _endpoint = value; }
        public string sequence { get => _sequence; set => _sequence = value; }
        public string nextSequence { get => _nextSequence; set => _nextSequence = value; }
        public string sequenceIfOK { get => _sequenceIfOK; set => _sequenceIfOK = value; }
        public string sequenceIfNotOK { get => _sequenceIfNotOK; set => _sequenceIfNotOK = value; }
        public List<bodyParameters> listParameters { get => _listParameters; set => _listParameters = value; }
    }

    public class bodyParameters
    {
        private string _origin;
        private string _field;
        private string _value;

        public string origin { get => _origin; set => _origin = value; }
        public string field { get => _field; set => _field = value; }
        public string value { get => _value; set => _value = value; }
    }
}
