﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models
{
    public class integration_entity
    {
        private string _endpoint;
        private string _verb;
        private bool _synchronous;
        private string _rules;

        public string endpoint { get => _endpoint; set => _endpoint = value; }
        public string verb { get => _verb; set => _verb = value; }
        public bool synchronous { get => _synchronous; set => _synchronous = value; }
        public string rules { get => _rules; set => _rules = value; }
    }
}
