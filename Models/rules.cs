﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motor.Parser.Models
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Parameter
    {
        public int origin { get; set; }
        public int order { get; set; }
        public string value { get; set; }
    }

    public class Query
    {
        public string sql { get; set; }
        public bool onlyOne { get; set; }
        public List<Parameter> parameters { get; set; }
    }

    public class NextSequence
    {
        public int sequenceIfOk { get; set; }
        public int sequenceIfNotOk { get; set; }
    }

    public class BodyParameter
    {
        public int origin { get; set; }
        public string field { get; set; }
        public object value { get; set; }
    }

    public class ServiceLayer
    {
        public string verb { get; set; }
        public string endpoint { get; set; }
        public List<BodyParameter> bodyParameters { get; set; }
    }

    public class rules
    {
        public string type { get; set; }
        public Query query { get; set; }
        public int sequence { get; set; }
        public NextSequence nextSequence { get; set; }
        public string conditionToSuccess { get; set; }
        public ServiceLayer serviceLayer { get; set; }
    }


}
